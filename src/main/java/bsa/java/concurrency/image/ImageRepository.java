package bsa.java.concurrency.image;

import bsa.java.concurrency.fs.FileSystem;
import bsa.java.concurrency.fs.HashedImage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class ImageRepository {
    @Autowired
    FileSystem fileSystem;

    public void updateStorage(List<HashedImage> images) {
        fileSystem.updateStorage(images);
    }

    public List<HashedImage> loadFromStorage() {
        return fileSystem.loadFromStorage();
    }

    public void save(String filename, byte[] bytes) {
        fileSystem.saveImage(filename, bytes);
    }

    public void delete(String imageName) {
        fileSystem.deleteImage(imageName);
    }

    public List<HashedImage> deleteFromStorage(List<HashedImage> persistentStorage, String imageName) {
        return fileSystem.deleteFromStorage(persistentStorage, imageName);
    }

    public void purge() {
        fileSystem.purgeImages();
    }
}
