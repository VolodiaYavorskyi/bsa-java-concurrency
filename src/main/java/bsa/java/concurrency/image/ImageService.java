package bsa.java.concurrency.image;

import bsa.java.concurrency.fs.HashedImage;
import bsa.java.concurrency.image.dto.SearchResultDTO;
import bsa.java.concurrency.image.service.DHasher;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;

@Service
public class ImageService {
    @Autowired
    private ImageRepository imageRepository;
    @Autowired
    private ExecutorService executorService;
    @Value("${image.root}")
    private String imageRoot;
    private static final Object storageLock = new Object();

    public void save(MultipartFile[] images) {
        CompletableFuture<?>[] futures = new CompletableFuture[images.length];
        for (int i = 0; i < images.length; i++) {
            MultipartFile mf = images[i];
            futures[i] = CompletableFuture.runAsync(() -> processImageSave(mf), executorService);
        }
        try {
            CompletableFuture.allOf(futures).get();
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
    }

    @SneakyThrows
    private void processImageSave(MultipartFile image) {
        String name = image.getOriginalFilename();
        byte[] bytes = image.getBytes();

        CompletableFuture<Void> fileSave = CompletableFuture.runAsync(
                () -> imageRepository.save(name, bytes));
        long hash = DHasher.calculateHash(bytes);

        fileSave.get();

        synchronized (storageLock) {
            List<HashedImage> persistentStorage = imageRepository.loadFromStorage();
            persistentStorage.add(new HashedImage(name, hash));
            imageRepository.updateStorage(persistentStorage);
        }
    }

    @SneakyThrows
    public List<SearchResultDTO> searchSimilar(MultipartFile image, double threshold) {
        List<SearchResultDTO> similar = new ArrayList<>();
        List<HashedImage> persistentStorage;
        synchronized (storageLock) {
            persistentStorage = imageRepository.loadFromStorage();
        }

        long hash = DHasher.calculateHash(image.getBytes());

        for (HashedImage hi : persistentStorage) {
            double matchPercent = DHasher.matchPercent(hash, hi.getHash());
            System.out.println(hi.getName() + " match: " + matchPercent);
            if (matchPercent > threshold) {
                similar.add(new SearchResultDTO(hi.getName(), matchPercent, imageRoot + hi.getName()));
            }
        }

        if (similar.isEmpty()) {
            processImageSave(image);
        }

        return similar;
    }

    public void deleteImage(String imageName) {
        imageRepository.delete(imageName);

        synchronized (storageLock) {
            List<HashedImage> persistentStorage = imageRepository.loadFromStorage();
            persistentStorage = imageRepository.deleteFromStorage(persistentStorage, imageName);
            imageRepository.updateStorage(persistentStorage);
        }
    }

    public void purge() {
        imageRepository.purge();
    }
}
