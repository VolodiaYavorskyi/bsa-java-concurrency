package bsa.java.concurrency.image.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.UUID;

@Data
@AllArgsConstructor
public class SearchResultDTO {
    private final String imageName;
    private final Double matchPercent;
    private final String imageUrl;
}
