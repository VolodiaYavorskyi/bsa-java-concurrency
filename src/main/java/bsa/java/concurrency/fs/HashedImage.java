package bsa.java.concurrency.fs;

import lombok.Value;

import java.io.Serializable;

@Value
public class HashedImage implements Serializable {
    String name;
    long hash;
}
