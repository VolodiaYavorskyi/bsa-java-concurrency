package bsa.java.concurrency.fs;

import org.apache.tomcat.util.http.fileupload.FileUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;

@Component
public class FileSystemImpl implements FileSystem {
    @Value("${image.root}")
    private String imageRoot;
    private final static String imageFormat = ".jpg";
    private final static String persistentStorageName = "storage.txt";

    @Override
    public CompletableFuture<String> saveImage(String filename, byte[] file) {
        String path = imageRoot + filename;
        if (!path.endsWith(imageFormat)) path += imageFormat;

        try (FileOutputStream fos = new FileOutputStream(path)) {
            fos.write(file);
        } catch (FileNotFoundException e) {
            System.err.println("File " + path + " cannot be created");
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return CompletableFuture.completedFuture(filename);
    }

    @Override
    public void updateStorage(List<HashedImage> images) {
        String path = imageRoot + persistentStorageName;

        try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(path))) {
            oos.writeObject(images);
        } catch (FileNotFoundException e) {
            System.err.println("File " + path + " cannot be created");
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public List<HashedImage> loadFromStorage() {
        String path = imageRoot + persistentStorageName;

        try (ObjectInputStream ois = new ObjectInputStream(new FileInputStream(path))) {
            @SuppressWarnings("unchecked")
            List<HashedImage> res = (List<HashedImage>) ois.readObject();
            return res;
        } catch (FileNotFoundException e) {
            System.err.println("File " + path + " not found, returning empty list");
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }

        return new ArrayList<>();
    }

    @Override
    public void deleteImage(String imageName) {
        String path = imageRoot + imageName;
        if (!path.endsWith(imageFormat)) path += imageFormat;

        try {
            Files.deleteIfExists(Path.of(path));
        } catch (IOException e) {
            System.err.println("IOException while deleting file " + path);
            e.printStackTrace();
        }
    }

    @Override
    public List<HashedImage> deleteFromStorage(List<HashedImage> persistentStorage, String imageName) {
        if (!imageName.endsWith(imageFormat)) imageName += imageFormat;

        List<HashedImage> res = new ArrayList<>();
        for (HashedImage hi : persistentStorage) {
            if (!hi.getName().equals(imageName)) {
                res.add(hi);
            }
        }

        return res;
    }

    @Override
    public void purgeImages() {
        String path = imageRoot;
        try {
            FileUtils.cleanDirectory(new File(path));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
